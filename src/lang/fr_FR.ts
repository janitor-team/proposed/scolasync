<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr" sourcelanguage="">
<context>
    <name>Aide</name>
    <message>
        <location filename="../help.ui" line="35"/>
        <source>Aide</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../help.ui" line="54"/>
        <source>Mode d&apos;emploi</source>
        <translation>Mode d&apos;emploi</translation>
    </message>
    <message>
        <location filename="../help.ui" line="95"/>
        <source>ScolaSync, pour gerer les fichiers des baladeurs</source>
        <translation>ScolaSync, pour gérer les fichiers des baladeurs</translation>
    </message>
    <message>
        <location filename="." line="5570656"/>
        <source>Numero de version&#xa0;:</source>
        <translation type="obsolete">Numéro de version :</translation>
    </message>
    <message>
        <location filename="../help.ui" line="122"/>
        <source>Auteurs</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="../help.ui" line="151"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../help.ui" line="180"/>
        <source>Langues et traductions</source>
        <translation>Langues et traductions</translation>
    </message>
    <message>
        <location filename="../help.ui" line="89"/>
        <source>A propos</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../help.ui" line="206"/>
        <source>Fermer</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../help.ui" line="71"/>
        <source>Manuel</source>
        <translation>Manuel</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../help.ui" line="102"/>
        <source>Numero de version :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="." line="5570656"/>
        <source>Choix des fichiers &#xe0; copier</source>
        <translation type="obsolete">Choix des fichiers à copier</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="24"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="43"/>
        <source>Ordinateur</source>
        <translation>Ordinateur</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="52"/>
        <source>Disque dur</source>
        <translation>Disque dur</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="330"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="134"/>
        <source>Baladeur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="157"/>
        <source>Disque flash</source>
        <translation>Disque flash</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="180"/>
        <source>Cle USB</source>
        <translation>Clé USB</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="304"/>
        <source>Ajouter a la liste</source>
        <translation>Ajouter à la liste</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="327"/>
        <source>Retirer de la liste</source>
        <translation>Retirer de la liste</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="379"/>
        <source>Abandonner</source>
        <translation>Abandonner</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="389"/>
        <source>Continuer ...</source>
        <translation>Continuer ...</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>Double-clic non pris en compte</source>
        <translation>Double-clic non pris en compte</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="567"/>
        <source>Veuillez choisir au moins un fichier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="563"/>
        <source>Choix de la destination ...</source>
        <translation type="unfinished">Choix de fichiers à copier</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="606"/>
        <source>Voir les copies</source>
        <translation>Voir les copies</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Destination manquante</source>
        <translation>Destination manquante</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Veuillez choisir une destination pour la copie des fichiers</source>
        <translation>Veuillez choisir une destination pour la copie des fichiers</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.ui" line="33"/>
        <source>Zone de recherche</source>
        <translation>Zone de recherche</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.ui" line="209"/>
        <source>Rechercher (fichier) ...</source>
        <translation>Rechercher (fichier) ...</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <source>Preferences de ScolaSync</source>
        <translation>Preferences de ScolaSync</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="24"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="98"/>
        <source>Travail</source>
        <translation>Travail</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="213"/>
        <source>inconnu</source>
        <translation>inconnu</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="212"/>
        <source>La cle %1&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation type="obsolete">La clé %1&lt;br&gt;n&apos;est pas identifiée, donnez le nom du propriétaire</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="251"/>
        <source>Entrer un nom</source>
        <translation>Entrer un nom</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="511"/>
        <source>Vous allez effacer plusieurs baladeurs</source>
        <translation>Vous allez effacer plusieurs baladeurs</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="136"/>
        <source>Choix de fichiers &#xe0; supprimer</source>
        <translation type="obsolete">Choix de fichiers à supprimer</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="138"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;R&#xe9;pertoire des &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documents de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Répertoire des &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documents de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="144"/>
        <source>Le r&#xe9;pertoire des cl&#xe9;s&lt;br&gt;o&#xf9; se trouvent les&lt;br&gt;documents de travail</source>
        <translation type="obsolete">Le répertoire des clés&lt;br&gt;où se trouvent les&lt;br&gt;documents de travail</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="146"/>
        <source>Choix de cl&#xe9; mod&#xe8;le</source>
        <translation type="obsolete">Choix de clé modèle</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="148"/>
        <source>Choisir une des cl&#xe9;s</source>
        <translation type="obsolete">Choisir une des clés</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="149"/>
        <source>Liste de fichiers &#xe0; traiter (jokers autoris&#xe9;s)</source>
        <translation type="obsolete">Liste de fichiers à traiter (jokers autorisés)</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="151"/>
        <source>Supprimer l&apos;item s&#xe9;lectionn&#xe9;</source>
        <translation type="obsolete">Supprimer l&apos;item sélectionné</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="162"/>
        <source>Rechercher (r&#xe9;pertoire) ...</source>
        <translation type="obsolete">Rechercher (répertoire) ...</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="95"/>
        <source>R&#xe9;pertoire pour les travaux&#xa0;:</source>
        <translation type="obsolete">Répertoire pour les travaux :</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="174"/>
        <source>R&#xe9;pertoire de destination des baladeurs</source>
        <translation type="obsolete">Répertoire de destination des baladeurs</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="176"/>
        <source>R&#xe9;pertoire de destination sur les baladeurs,&lt;br&gt;peut &#xea;tre modifi&#xe9; dans les pr&#xe9;f&#xe9;rences</source>
        <translation type="obsolete">Répertoire de destination sur les baladeurs,&lt;br&gt;peut être modifié dans les préférences</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="186"/>
        <source>Taille totale&#xa0;:</source>
        <translation type="obsolete">Taille totale :</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="506"/>
        <source>Choix de fichiers &#xe0; supprimer (jokers autoris&#xe9;s)</source>
        <translation type="obsolete">Choix de fichiers à supprimer (jokers autorisés)</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="569"/>
        <source>Aucun fichier s&#xe9;lectionn&#xe9;</source>
        <translation type="obsolete">Aucun fichier sélectionné</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="564"/>
        <source>Choix de fichiers &#xe0; copier depuis les baladeurs</source>
        <translation type="obsolete">Choix de fichiers à copier depuis les baladeurs</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="577"/>
        <source>Choisir un r&#xe9;pertoire de destination</source>
        <translation type="obsolete">Choisir un répertoire de destination</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="608"/>
        <source>Voulez-vous voir les fichiers copi&#xe9;s&#xa0;?</source>
        <translation type="obsolete">Voulez-vous voir les fichiers copiés ?</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="97"/>
        <source>Fichier sp&#xe9;cial pour le manuel :</source>
        <translation type="obsolete">Fichier spécial pour le manuel :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="112"/>
        <source>/usr/share/scolasync/help/manualPage_fr_FR.html</source>
        <translation>/usr/share/scolasync/help/manualPage_fr_FR.html</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="157"/>
        <source>Ligne d&apos;&#xe9;dition du nom d&apos;un fichier ou d&apos;un r&#xe9;pertoire.&lt;br&gt;Jokers autoris&#xe9;s</source>
        <translation type="obsolete">Ligne d&apos;édition du nom d&apos;un fichier ou d&apos;un répertoire.&lt;br&gt;Jokers autorisés</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="154"/>
        <source>Ajouter un fichier ou un filtre d&#xe9;fini dans la ligne d&apos;&#xe9;dition</source>
        <translation type="obsolete">Ajouter un fichier ou un filtre défini dans la ligne d&apos;édition</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="639"/>
        <source>R&#xe9;it&#xe9;rer la derni&#xe8;re commande</source>
        <translation type="obsolete">Réitérer la dernière commande</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="93"/>
        <source>Efface les fichiers et les r&#xe9;pertoires apr&#xe8;s copie</source>
        <translation type="obsolete">Efface les fichiers et les répertoires après copie</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="91"/>
        <source>Si la case est coch&#xe9;e,&lt;br&gt;les donn&#xe9;es sont transf&#xe9;r&#xe9;es sur le disque dur&lt;br&gt;puis effac&#xe9;e des baladeurs.</source>
        <translation type="obsolete">Si la case est cochée,&lt;br&gt;les données sont transférées sur le disque dur&lt;br&gt;puis effacée des baladeurs.</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="92"/>
        <source>Si la case est coch&#xe9;e, les donn&#xe9;es sont transf&#xe9;r&#xe9;es sur le disque dur puis effac&#xe9;e des baladeurs.</source>
        <translation type="obsolete">Si la case est cochée, les données sont transférées sur le disque dur puis effacée des baladeurs.</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="111"/>
        <source>Choix d&apos;un groupe d&apos;&#xe9;l&#xe8;ves</source>
        <translation type="obsolete">Choix d&apos;un groupe d&apos;élèves</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="65"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="71"/>
        <source>Replier toutes les classes</source>
        <translation>Replier toutes les classes</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="74"/>
        <source>Tout replier</source>
        <translation>Tout replier</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="117"/>
        <source>Cocher tous les &#xe9;l&#xe8;ves visibles</source>
        <translation type="obsolete">Cocher tous les élèves visibles</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="88"/>
        <source>Cocher</source>
        <translation>Cocher</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="119"/>
        <source>D&#xe9;cocher tous les &#xe9;l&#xe8;ves, visibles ou non</source>
        <translation type="obsolete">Décocher tous les élèves, visibles ou non</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="120"/>
        <source>D&#xe9;cocher</source>
        <translation type="obsolete">Décocher</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="122"/>
        <source>Ajout &#xe0; la liste</source>
        <translation type="obsolete">Ajout à la liste</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="135"/>
        <source>Suppr. de la liste</source>
        <translation>Suppr. de la liste</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="185"/>
        <source>Valider</source>
        <translation>Valider</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="126"/>
        <source>N&#xb0; &#xe0; partir de</source>
        <translation type="obsolete">N° à partir de</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="125"/>
        <source>Quand la case est coch&#xe9;e, un num&#xe9;ro sera ajout&#xe9; comme pr&#xe9;fixe aux noms</source>
        <translation type="obsolete">Quand la case est cochée, un numéro sera ajouté comme préfixe aux noms</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="127"/>
        <source>La num&#xe9;rotation commence &#xe0; cette valeur</source>
        <translation type="obsolete">La numérotation commence à cette valeur</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="121"/>
        <source>Ajouter les noms coch&#xe9;s de l&apos;arbre dans la liste &#xe0; droite</source>
        <translation type="obsolete">Ajouter les noms cochés de l&apos;arbre dans la liste à droite</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="132"/>
        <source>Supprimer de la liste les noms en surbrillance</source>
        <translation>Supprimer de la liste les noms en surbrillance</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="182"/>
        <source>Accepter la liste actuelle et fermer ce dialogue</source>
        <translation>Accepter la liste actuelle et fermer ce dialogue</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="192"/>
        <source>Supprimer tous les noms de la liste et fermer ce dialogue</source>
        <translation>Supprimer tous les noms de la liste et fermer ce dialogue</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="131"/>
        <source>Remettre &#xe0; z&#xe9;ro</source>
        <translation type="obsolete">Remettre à zéro</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="70"/>
        <source>Choix du propri&#xe9;taire</source>
        <translation type="obsolete">Choix du propriétaire</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="71"/>
        <source>Nouveau nom du propri&#xe9;taire du baladeur</source>
        <translation type="obsolete">Nouveau nom du propriétaire du baladeur</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="112"/>
        <source>Liste d&apos;&#xe9;l&#xe8;ves</source>
        <translation type="obsolete">Liste d&apos;élèves</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="26"/>
        <source>Ouvrir un fichier ...</source>
        <translation>Ouvrir un fichier ...</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="94"/>
        <source>Fichier des &#xe9;l&#xe8;ves :</source>
        <translation type="obsolete">Fichier des élèves :</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="67"/>
        <source>Nommer le baladeur nouvellement connect&#xe9;</source>
        <translation type="obsolete">Nommer le baladeur nouvellement connecté</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="39"/>
        <source>Changez le choix parmi cette liste si une autre ligne convient mieux</source>
        <translation>Changez le choix parmi cette liste si une autre ligne convient mieux</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="56"/>
        <source>nom actuel du baladeur</source>
        <translation>nom actuel du baladeur</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="71"/>
        <source>C&apos;est le nom connu par votre ordinateur, s&apos;il a d&#xe9;j&#xe0; &#xe9;t&#xe9; d&#xe9;fini dans le pass&#xe9;</source>
        <translation type="obsolete">C&apos;est le nom connu par votre ordinateur, s&apos;il a déjà été défini dans le passé</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="72"/>
        <source>nom propos&#xe9;</source>
        <translation type="obsolete">nom proposé</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="73"/>
        <source>Le nouveau nom propos&#xe9; peut venir de la liste &#xe0; gauche ou &#xea;tre modifi&#xe9; &#xe0; la main</source>
        <translation type="obsolete">Le nouveau nom proposé peut venir de la liste à gauche ou être modifié à la main</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="113"/>
        <source>Renommer le baladeur</source>
        <translation>Renommer le baladeur</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="116"/>
        <source>Choisir comme nouveau nom</source>
        <translation>Choisir comme nouveau nom</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="123"/>
        <source>Fermer le dialogue sans rien faire</source>
        <translation>Fermer le dialogue sans rien faire</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="77"/>
        <source>&#xc9;chappement</source>
        <translation type="obsolete">Échappement</translation>
    </message>
    <message>
        <location filename="../choixEleves.py" line="93"/>
        <source>Le fichier {schoolfile} n&apos;a pas pu &#xc3;&#xaa;tre trait&#xc3;&#xa9; : {erreur}</source>
        <translation type="unfinished">Le fichier {schoolfile} n&apos;a pas pu être traité : {erreur}</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="250"/>
        <source>La cle {id}&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation>La cle {id}&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>pas d&apos;action pour l&apos;attribut {a}</source>
        <translation>pas d&apos;action pour l&apos;attribut {a}</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="639"/>
        <source>La derni&#xe8;re commande &#xe9;tait&lt;br&gt;{cmd}&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs&#xa0;?</source>
        <translation type="obsolete">La dernière commande était&lt;br&gt;{cmd}&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs ?</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="162"/>
        <source>%s kilo-octets</source>
        <translation>%s kilo-octets</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="165"/>
        <source>%s m&#xc3;&#xa9;ga-octets</source>
        <translation type="unfinished">%s méga-octets</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="168"/>
        <source>%s giga-octets</source>
        <translation>%s giga-octets</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="201"/>
        <source>Choissez un fichier (ou plus)</source>
        <translation>Choissez un fichier (ou plus)</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="205"/>
        <source>Choissez un r&#xc3;&#xa9;pertoire</source>
        <translation type="unfinished">Choissez un répertoire</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="220"/>
        <source>Aucune cl&#xc3;&#xa9; mod&#xc3;&#xa8;le s&#xc3;&#xa9;lectionn&#xc3;&#xa9;e</source>
        <translation type="unfinished">Aucune clé modèle sélectionnée</translation>
    </message>
    <message>
        <location filename="../preferences.py" line="50"/>
        <source>{t} secondes</source>
        <translation>{t} secondes</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="503"/>
        <source>Choix de fichiers à supprimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="39"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Répertoire des &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documents de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="57"/>
        <source>Le répertoire des clés&lt;br&gt;où se trouvent les&lt;br&gt;documents de travail</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="60"/>
        <source>Le répertoire des clés où se trouvent les documents de travail</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="67"/>
        <source>Choix de clé modèle</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="83"/>
        <source>Choisir une des clés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="105"/>
        <source>Liste de fichiers à traiter (jokers autorisés)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="144"/>
        <source>Supprimer l&apos;item sélectionné</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="167"/>
        <source>Ajouter un fichier ou un filtre défini dans la ligne d&apos;édition</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="193"/>
        <source>Ligne d&apos;édition du nom d&apos;un fichier ou d&apos;un répertoire.&lt;br&gt;Jokers autorisés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="232"/>
        <source>Rechercher (répertoire) ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="45"/>
        <source>Si la case est cochée,&lt;br&gt;les données sont transférées sur le disque dur&lt;br&gt;puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="48"/>
        <source>Si la case est cochée, les données sont transférées sur le disque dur puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="51"/>
        <source>Efface les fichiers et les répertoires après copie</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="77"/>
        <source>Fichier des élèves :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="91"/>
        <source>Répertoire pour les travaux :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="105"/>
        <source>Fichier spécial pour le manuel :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="14"/>
        <source>Nommer le baladeur nouvellement connecté</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="66"/>
        <source>C&apos;est le nom connu par votre ordinateur, s&apos;il a déjà été défini dans le passé</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="86"/>
        <source>nom proposé</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="93"/>
        <source>Le nouveau nom proposé peut venir de la liste à gauche ou être modifié à la main</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="126"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="14"/>
        <source>Choix d&apos;un groupe d&apos;élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="20"/>
        <source>Liste d&apos;élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="85"/>
        <source>Cocher tous les élèves visibles</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="95"/>
        <source>Décocher tous les élèves, visibles ou non</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="98"/>
        <source>Décocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="118"/>
        <source>Ajouter les noms cochés de l&apos;arbre dans la liste à droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="121"/>
        <source>Ajout à la liste</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="162"/>
        <source>Quand la case est cochée, un numéro sera ajouté comme préfixe aux noms</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="165"/>
        <source>N° à partir de</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="172"/>
        <source>La numérotation commence à cette valeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="195"/>
        <source>Remettre à zéro</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.ui" line="14"/>
        <source>Choix des fichiers à copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.ui" line="82"/>
        <source>Répertoire de destination des baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.ui" line="115"/>
        <source>Répertoire de destination sur les baladeurs,&lt;br&gt;peut être modifié dans les préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.ui" line="266"/>
        <source>Taille totale :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="504"/>
        <source>Choix de fichiers à supprimer (jokers autorisés)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="567"/>
        <source>Aucun fichier sélectionné</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="561"/>
        <source>Choix de fichiers à copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="562"/>
        <source>Choix de fichiers à copier depuis les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="575"/>
        <source>Choisir un répertoire de destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="606"/>
        <source>Voulez-vous voir les fichiers copiés ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="637"/>
        <source>Réitérer la dernière commande</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="637"/>
        <source>La dernière commande était&lt;br&gt;{cmd}&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ownedUsbDisk.py" line="69"/>
        <source>Choix du propriétaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ownedUsbDisk.py" line="70"/>
        <source>Nouveau nom du propriétaire du baladeur</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../mainWindow.py" line="673"/>
        <source>D&#xe9;montage des baladeurs</source>
        <translation type="obsolete">Démontage des baladeurs</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="673"/>
        <source>&#xca;tes-vous s&#xfb;r de vouloir d&#xe9;monter tous les baladeurs coch&#xe9;s de la liste&#xa0;?</source>
        <translation type="obsolete">Êtes-vous sûr de vouloir démonter tous les baladeurs cochés de la liste ?</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="776"/>
        <source>Point de montage de la cl&#xc3;&#xa9; USB ou du baladeur&#xc2;&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir les fichiers.</source>
        <translation type="unfinished">Propriétaire de la clé USB ou du baladeur ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour modifier.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="778"/>
        <source>Capacit&#xc3;&#xa9; de la cl&#xc3;&#xa9; USB ou du baladeur en kO&#xc2;&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occup&#xc3;&#xa9;e.</source>
        <translation type="unfinished">Point de montage de la clé USB ou du baladeur ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir les fichiers.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="780"/>
        <source>Capacit&#xe9; de la cl&#xe9; USB ou du baladeur en kO&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occup&#xe9;e.</source>
        <translation type="obsolete">Capacité de la clé USB ou du baladeur en kO ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occupée.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="782"/>
        <source>Mod&#xc3;&#xa8;le de la cl&#xc3;&#xa9; USB ou du baladeur.</source>
        <translation type="unfinished">Fabricant de la clé USB ou du baladeur.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="784"/>
        <source>Num&#xc3;&#xa9;ro de s&#xc3;&#xa9;rie de la cl&#xc3;&#xa9; USB ou du baladeur.</source>
        <translation type="unfinished">Modèle de la clé USB ou du baladeur.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="786"/>
        <source>Num&#xe9;ro de s&#xe9;rie de la cl&#xe9; USB ou du baladeur.</source>
        <translation type="obsolete">Numéro de série de la clé USB ou du baladeur.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="774"/>
        <source>Propri&#xc3;&#xa9;taire de la cl&#xc3;&#xa9; USB ou du baladeur&#xc2;&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour modifier.</source>
        <translation type="unfinished">Cocher ou décocher cette case en cliquant.&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour agir sur plusieurs baladeurs.</translation>
    </message>
    <message>
        <location filename="../help.py" line="40"/>
        <source>Version num&#xc3;&#xa9;ro {major}.{minor}</source>
        <translation type="unfinished">Version numéro {major}.{minor}</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="671"/>
        <source>Démontage des baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="671"/>
        <source>Êtes-vous sûr de vouloir démonter tous les baladeurs cochés de la liste ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="772"/>
        <source>Cocher ou décocher cette case en cliquant.&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour agir sur plusieurs baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="780"/>
        <source>Fabricant de la clé USB ou du baladeur.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../diskFull.ui" line="14"/>
        <source>Disk size</source>
        <translation>Taille du disque</translation>
    </message>
    <message>
        <location filename="../diskFull.ui" line="25"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../diskFull.ui" line="39"/>
        <source>total size</source>
        <translation>taille totale</translation>
    </message>
    <message>
        <location filename="../diskFull.ui" line="46"/>
        <source>Used</source>
        <translation>Utilisé(s)</translation>
    </message>
    <message>
        <location filename="../mainWindow.ui" line="14"/>
        <source>ScolaSync</source>
        <translation>ScolaSync</translation>
    </message>
    <message>
        <location filename="../mainWindow.ui" line="32"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="42"/>
        <source>Copier depuis les cles</source>
        <translation type="obsolete">Copier depuis les clés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="54"/>
        <source>Copier vers les cles</source>
        <translation type="obsolete">Copier vers les clés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="78"/>
        <source>Demonter les cles</source>
        <translation type="obsolete">Démonter les clés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="90"/>
        <source>Preferences</source>
        <translation type="obsolete">Préférences</translation>
    </message>
    <message>
        <location filename="../mainWindow.ui" line="324"/>
        <source>Aide</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="172"/>
        <source>Force &#xe0; recompter les baladeurs</source>
        <translation type="obsolete">Force à recompter les baladeurs</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="170"/>
        <source>Affiche le nombre de baladeurs connect&#xe9;s</source>
        <translation type="obsolete">Affiche le nombre de baladeurs connectés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="175"/>
        <source>Pr&#xe9;f&#xe9;rences</source>
        <translation type="obsolete">Préférences</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="165"/>
        <source>Refaire &#xe0; nouveau</source>
        <translation type="obsolete">Refaire à nouveau</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="166"/>
        <source>Refaire &#xe0; nouveau la derni&#xe8;re op&#xe9;ration r&#xe9;ussie, avec les baladeurs connect&#xe9;s plus r&#xe9;cemment</source>
        <translation type="obsolete">Refaire à nouveau la dernière opération réussie, avec les baladeurs connectés plus récemment</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="163"/>
        <source>&#xc9;jecter les baladeurs s&#xe9;lectionn&#xe9;s</source>
        <translation type="obsolete">Éjecter les baladeurs sélectionnés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="154"/>
        <source>Copier depuis les baladeurs s&#xe9;lectionn&#xe9;s</source>
        <translation type="obsolete">Copier depuis les baladeurs sélectionnés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="157"/>
        <source>Copier vers les baladeurs s&#xe9;lectionn&#xe9;s</source>
        <translation type="obsolete">Copier vers les baladeurs sélectionnés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="160"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs s&#xe9;lectionn&#xe9;s</source>
        <translation type="obsolete">Effacer des fichiers ou des dossiers dans les baladeurs sélectionnés</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="303"/>
        <source>Arr&#xea;ter les op&#xe9;rations en cours</source>
        <translation type="obsolete">Arrêter les opérations en cours</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="304"/>
        <source>Essaie d&apos;arr&#xea;ter les op&#xe9;rations en cours. &#xc0; faire seulement si celles-ci durent trop longtemps</source>
        <translation type="obsolete">Essaie d&apos;arrêter les opérations en cours. À faire seulement si celles-ci durent trop longtemps</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="100"/>
        <source>&lt;br /&gt;Des noms sont disponibles pour renommer les prochains baladeurs que vous brancherez</source>
        <translation>&lt;br /&gt;Des noms sont disponibles pour renommer les prochains baladeurs que vous brancherez</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="101"/>
        <source>&lt;br /&gt;Cliquez sur ce bouton pour pr&#xc3;&#xa9;parer une liste de noms afin de renommer les prochains baladeurs que vous brancherez</source>
        <translation type="unfinished">&lt;br /&gt;Cliquez sur ce bouton pour préparer une liste de noms afin de renommer les prochains baladeurs que vous brancherez</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="44"/>
        <source>Copier depuis les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="71"/>
        <source>Copier vers les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="98"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="125"/>
        <source>Éjecter les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="299"/>
        <source>Refaire à nouveau</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="300"/>
        <source>Refaire à nouveau la dernière opération réussie, avec les baladeurs connectés plus récemment</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="250"/>
        <source>Affiche le nombre de baladeurs connectés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="270"/>
        <source>Force à recompter les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="297"/>
        <source>Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="301"/>
        <source>Arrêter les opérations en cours</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="302"/>
        <source>Essaie d&apos;arrêter les opérations en cours. À faire seulement si celles-ci durent trop longtemps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>checkBoxDialog</name>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="50"/>
        <source>Gestion des cases &#xe0; cocher</source>
        <translation type="obsolete">Gestion des cases à cocher</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="27"/>
        <source>Cocher tous les baladeurs</source>
        <translation>Cocher tous les baladeurs</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="30"/>
        <source>Tout cocher</source>
        <translation>Tout cocher</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="55"/>
        <source>Inverser le choix de baladeurs&lt;br&gt;Les baladeurs coch&#xe9;s seront d&#xe9;coch&#xe9;s, et inversement</source>
        <translation type="obsolete">Inverser le choix de baladeurs&lt;br&gt;Les baladeurs cochés seront décochés, et inversement</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="47"/>
        <source>Inverser le choix</source>
        <translation>Inverser le choix</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="58"/>
        <source>D&#xe9;s&#xe9;lectionner les baladeurs</source>
        <translation type="obsolete">Désélectionner les baladeurs</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="64"/>
        <source>Ne rien cocher</source>
        <translation>Ne rien cocher</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="78"/>
        <source>Ne rien faire</source>
        <translation>Ne rien faire</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="62"/>
        <source>&#xc9;chappement</source>
        <translation type="obsolete">Échappement</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../checkBoxDialog.ui" line="14"/>
        <source>Gestion des cases à cocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../checkBoxDialog.ui" line="44"/>
        <source>Inverser le choix de baladeurs&lt;br&gt;Les baladeurs cochés seront décochés, et inversement</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../checkBoxDialog.ui" line="61"/>
        <source>Désélectionner les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../checkBoxDialog.ui" line="81"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>diskFull</name>
    <message>
        <location filename="../diskFull.py" line="49"/>
        <source>Place totale&#xc2;&#xa0;: {size} kilo-octets</source>
        <translation type="unfinished">Place totale : {size} kilo-octets</translation>
    </message>
    <message>
        <location filename="../diskFull.py" line="50"/>
        <source>Place utilis&#xc3;&#xa9;e&#xc2;&#xa0;: {size} kilo-octets</source>
        <translation type="unfinished">Place utilisée : {size} kilo-octets</translation>
    </message>
</context>
<context>
    <name>uDisk</name>
    <message>
        <location filename="../usbDisk2.py" line="423"/>
        <source>point de montage</source>
        <translation>point de montage</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="424"/>
        <source>taille</source>
        <translation>taille</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="430"/>
        <source>cocher</source>
        <translation>cocher</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="201"/>
        <source>owner</source>
        <translation>propriétaire</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="429"/>
        <source>mod&#xe8;le de disque</source>
        <translation type="obsolete">modèle de disque</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="430"/>
        <source>num&#xe9;ro de s&#xe9;rie</source>
        <translation type="obsolete">numéro de série</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="425"/>
        <source>marque</source>
        <translation>marque</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="284"/>
        <source>Partition ajout&#xe9;e %s</source>
        <translation type="obsolete">Partition ajoutée %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="300"/>
        <source>&#xc9;chec au montage du disque : %s</source>
        <translation type="obsolete">Échec au montage du disque : %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="331"/>
        <source>Disque ajout&#xe9; : %s</source>
        <translation type="obsolete">Disque ajouté : %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="358"/>
        <source>Changement pour le disque %s</source>
        <translation>Changement pour le disque %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="370"/>
        <source>Disque d&#xe9;branch&#xe9; du syst&#xe8;me : %s</source>
        <translation type="obsolete">Disque débranché du système : %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="339"/>
        <source>On n&apos;ajoute pas le disque : partition non-USB</source>
        <translation>On n&apos;ajoute pas le disque : partition non-USB</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="304"/>
        <source>On n&apos;ajoute pas le disque : partition vide</source>
        <translation>On n&apos;ajoute pas le disque : partition vide</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="329"/>
        <source>Disque d&#xe9;j&#xe0; ajout&#xe9; auparavant : %s</source>
        <translation type="obsolete">Disque déjà ajouté auparavant : %s</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="282"/>
        <source>Partition ajoutée %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="297"/>
        <source>Échec au montage du disque : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="326"/>
        <source>Disque déjà ajouté auparavant : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="328"/>
        <source>Disque ajouté : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="367"/>
        <source>Disque débranché du système : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="426"/>
        <source>modèle de disque</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="427"/>
        <source>numéro de série</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
