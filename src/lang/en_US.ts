<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="en_US" sourcelanguage="">
<context>
    <name>Aide</name>
    <message>
        <location filename="../help.ui" line="35"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../help.ui" line="54"/>
        <source>Mode d&apos;emploi</source>
        <translation>Usage</translation>
    </message>
    <message>
        <location filename="../help.ui" line="95"/>
        <source>ScolaSync, pour gerer les fichiers des baladeurs</source>
        <translation>Scolasync, to manage files in pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="7274458"/>
        <source>Numero de version&#xa0;:</source>
        <translation type="obsolete">Version number:</translation>
    </message>
    <message>
        <location filename="../help.ui" line="122"/>
        <source>Auteurs</source>
        <translation>Authors</translation>
    </message>
    <message>
        <location filename="../help.ui" line="151"/>
        <source>Licence</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="../help.ui" line="180"/>
        <source>Langues et traductions</source>
        <translation>Languages and translations</translation>
    </message>
    <message>
        <location filename="../help.ui" line="89"/>
        <source>A propos</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../help.ui" line="206"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../help.ui" line="71"/>
        <source>Manuel</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="154"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;Le manuel de Scolasync peut &#xea;tre consult&#xe9; de plusieurs fa&#xe7;ons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:8px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dans un manuel tr&#xe8;s court, int&#xe9;gr&#xe9; au paquet :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;file:///usr/share/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cliquez ici&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dans un manuel plus riche, &#xe0; installer &#xe0; part :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;file:///usr/share/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cliquez ici&lt;/span&gt;&lt;/a&gt; (le choix du manuel peut &#xea;tre chang&#xe9; &#xe0; l&apos;aide des &lt;span style=&quot; font-style:italic;&quot;&gt;pr&#xe9;f&#xe9;rences&lt;/span&gt;).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dans un manuel en ligne :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.ofset.org/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cliquez ici.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;Solasync&apos;s manual can be accessed in a few ways:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:8px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;From a short user manual, integrated with the package:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;file:///usr/share/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cliquez ici&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dans un manuel plus riche, à installer à part :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;file:///usr/share/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;click here&lt;/span&gt;&lt;/a&gt; (the manual&apos;s choice can be changed with the &lt;span style=&quot; font-style:italic;&quot;&gt;preference dialog&lt;/span&gt;).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;From an inline manual:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.ofset.org/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;click here.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../help.ui" line="102"/>
        <source>Numero de version :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../Ui_help.py" line="7274304"/>
        <source>Choix des fichiers &#xe0; copier</source>
        <translation type="obsolete">Choose files to copy</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="24"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="43"/>
        <source>Ordinateur</source>
        <translation>Desktop/Laptop</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="52"/>
        <source>Disque dur</source>
        <translation>Hard disk</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="330"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="7274310"/>
        <source>R&#xe9;pertoire de destination des baladeurs</source>
        <translation type="obsolete">Destination directory in the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="7274310"/>
        <source>R&#xe9;pertoire de destination sur les baladeurs,&lt;br&gt;peut &#xea;tre modifi&#xe9; dans les pr&#xe9;f&#xe9;rences</source>
        <translation type="obsolete">Destination directory,&lt;br&gt;can be modified in the preferences</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="134"/>
        <source>Baladeur</source>
        <translation>Puggable device</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="157"/>
        <source>Disque flash</source>
        <translation>Flash disk</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="180"/>
        <source>Cle USB</source>
        <translation>USB stick</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="7274388"/>
        <source>Taille totale&#xa0;:</source>
        <translation type="obsolete">Total size:</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="304"/>
        <source>Ajouter a la liste</source>
        <translation>Add to the list</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="327"/>
        <source>Retirer de la liste</source>
        <translation>Remove from the list</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="379"/>
        <source>Abandonner</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.ui" line="389"/>
        <source>Continuer ...</source>
        <translation>Continue...</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>Double-clic non pris en compte</source>
        <translation>Double click is ignored</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="280"/>
        <source>pas d&apos;action pour l&apos;attribut %1</source>
        <translation type="obsolete">No action for attribute &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7274270"/>
        <source>Choix de fichiers &#xe0; supprimer</source>
        <translation type="obsolete">Choose files to delete</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7274270"/>
        <source>Choix de fichiers &#xe0; supprimer (jokers autoris&#xe9;s)</source>
        <translation type="obsolete">Choose files to delete (wildcards are OK)</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="0"/>
        <source>Aucun fichier s&#xe9;lectionn&#xe9;</source>
        <translation type="obsolete">No selected file</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="567"/>
        <source>Veuillez choisir au moins un fichier</source>
        <translation>Please choose at least one file</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7274030"/>
        <source>Choix de fichiers &#xe0; copier</source>
        <translation type="obsolete">Choose files to copy</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7274030"/>
        <source>Choix de fichiers &#xe0; copier depuis les baladeurs</source>
        <translation type="obsolete">Choose files to copy from the pluggable media</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="563"/>
        <source>Choix de la destination ...</source>
        <translation>Choose destination...</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7274038"/>
        <source>Choisir un r&#xe9;pertoire de destination</source>
        <translation type="obsolete">Choose a destination directory</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="606"/>
        <source>Voir les copies</source>
        <translation>See the copies</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7273976"/>
        <source>Voulez-vous voir les fichiers copi&#xe9;s&#xa0;?</source>
        <translation type="obsolete">Do you want to see the copied files?</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Destination manquante</source>
        <translation>Missing destination</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Veuillez choisir une destination pour la copie des fichiers</source>
        <translation>Please choose a destination to copy the files</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7273966"/>
        <source>Liste de p&#xe9;riph&#xe9;riques modifi&#xe9;e pour Scolasync</source>
        <translation type="obsolete">Device list modified for Scolasyn</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7273966"/>
        <source>Une cl&#xe9; USB ou un baladeur num&#xe9;rique au moins a change de statut. V&#xe9;rifiez &#xe9;ventuellement la liste des p&#xe9;riph&#xe9;riques de Scolasync</source>
        <translation type="obsolete">At least one USB stick or music player has chaged its status; eventually check the list of devices of Scolasync</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.ui" line="33"/>
        <source>Zone de recherche</source>
        <translation>Search zone</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="7274512"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;R&#xe9;pertoire des &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documents de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Directory of the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;work documents&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="7274512"/>
        <source>Le r&#xe9;pertoire des cl&#xe9;s&lt;br&gt;o&#xf9; se trouvent les&lt;br&gt;documents de travail</source>
        <translation type="obsolete">Les direstory of the media&lt;br&gt;where the work&lt;br&gt;documents are</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="0"/>
        <source>Le r&#xe9;pertoire des cl&#xe9;s o&#xf9; se trouvent les documents de travail</source>
        <translation type="obsolete">The directory of the media where the work documents are</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="0"/>
        <source>Choix de cl&#xe9; mod&#xe8;le</source>
        <translation type="obsolete">Choose model pluggable medium</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="0"/>
        <source>Choisir une des cl&#xe9;s</source>
        <translation type="obsolete">Choose one pluggable medium</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="0"/>
        <source>Liste de fichiers &#xe0; traiter (jokers autoris&#xe9;s)</source>
        <translation type="obsolete">List of files to manage (wildcards are OK)</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="0"/>
        <source>Supprimer l&apos;item s&#xe9;lectionn&#xe9;</source>
        <translation type="obsolete">Delete the selected item</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="0"/>
        <source>&#xc9;crire le nom d&apos;un fichier ou d&apos;un r&#xe9;pertoire.&lt;br&gt;Jokers autoris&#xe9;s</source>
        <translation type="obsolete">Write the name of a file or a directory&lt;br&gt;wildcards are OK</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="150"/>
        <source>Ajouter un fichier ou un filtre</source>
        <translation type="obsolete">Add a filename or a filter</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.ui" line="209"/>
        <source>Rechercher (fichier) ...</source>
        <translation>Search (file)...</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="7274332"/>
        <source>Rechercher (r&#xe9;pertoire) ...</source>
        <translation type="obsolete">Search (directory)...</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <source>Preferences de ScolaSync</source>
        <translation>Preferences of Scolasync</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="24"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="7274524"/>
        <source>Les cases &#xe0; cocher permettent de n&apos;agir que sur une part des baladeurs</source>
        <translation type="obsolete">Checkboxes allow to deal with a subset of the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="7274524"/>
        <source>Utiliser des cases &#xe0; cocher pour d&#xe9;signer les baladeurs</source>
        <translation type="obsolete">Use checkboxes to select pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="0"/>
        <source>R&#xe9;pertoire pour les travaux&#xa0;:</source>
        <translation type="obsolete">Directory for the homeworks:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="98"/>
        <source>Travail</source>
        <translation>Homework</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="162"/>
        <source>%s kilo-octets</source>
        <translation>%s kBytes</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="7274256"/>
        <source>%s m&#xe9;ga-octets</source>
        <translation type="obsolete">%s MBytes</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="168"/>
        <source>%s giga-octets</source>
        <translation>%s GBytes</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="213"/>
        <source>inconnu</source>
        <translation>**unknown**</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="212"/>
        <source>La cle %1&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation type="obsolete">The medium %1&lt;br&gt;is unknown, please give the owner&apos;s name</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="251"/>
        <source>Entrer un nom</source>
        <translation>Enter a name</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="511"/>
        <source>Vous allez effacer plusieurs baladeurs</source>
        <translation>You will erase many drives</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="97"/>
        <source>Fichier sp&#xe9;cial pour le manuel :</source>
        <translation type="obsolete">Spécial file location for the manual:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="112"/>
        <source>/usr/share/scolasync/help/manualPage_fr_FR.html</source>
        <translation>/usr/share/scolasync/help/manualPage_en_US.html</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="127"/>
        <source>Recomptage automatique des m&#xe9;dias :</source>
        <translation type="obsolete">Automatic counting of the medias:</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="128"/>
        <source>toutes les</source>
        <translation type="obsolete">every</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="129"/>
        <source>30 secondes</source>
        <translation type="obsolete">30 seconds</translation>
    </message>
    <message>
        <location filename="../preferences.py" line="53"/>
        <source>%1 secondes</source>
        <translation type="obsolete">%1 seconds</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="157"/>
        <source>Ligne d&apos;&#xe9;dition du nom d&apos;un fichier ou d&apos;un r&#xe9;pertoire.&lt;br&gt;Jokers autoris&#xe9;s</source>
        <translation type="obsolete">Edit line for the name of a file or a directory&lt;br&gt;wildcards are OK</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="154"/>
        <source>Ajouter un fichier ou un filtre d&#xe9;fini dans la ligne d&apos;&#xe9;dition</source>
        <translation type="obsolete">Add a filename or a filter defined in the edit line</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="639"/>
        <source>R&#xe9;it&#xe9;rer la derni&#xe8;re commande</source>
        <translation type="obsolete">Redo the last command</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="547"/>
        <source>La derni&#xe8;re commande &#xe9;tait&lt;br&gt;%1&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs&#xa0;?</source>
        <translation type="obsolete">The last command was&lt;br&gt;%1&lt;br&gt;Do you want to redo it with the new media?</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="93"/>
        <source>Efface les fichiers et les r&#xe9;pertoires apr&#xe8;s copie</source>
        <translation type="obsolete">Delete files and directories after copying them</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="91"/>
        <source>Si la case est coch&#xe9;e,&lt;br&gt;les donn&#xe9;es sont transf&#xe9;r&#xe9;es sur le disque dur&lt;br&gt;puis effac&#xe9;e des baladeurs.</source>
        <translation type="obsolete">If this box is checked,&lt;br&gt;data are transferred to the disk,&lt;br&gt;then delete from the pluggable media.</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="92"/>
        <source>Si la case est coch&#xe9;e, les donn&#xe9;es sont transf&#xe9;r&#xe9;es sur le disque dur puis effac&#xe9;e des baladeurs.</source>
        <translation type="obsolete">If this box is checked, data are transferred to the disk, then delete from the pluggable media.</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="111"/>
        <source>Choix d&apos;un groupe d&apos;&#xe9;l&#xe8;ves</source>
        <translation type="obsolete">Choose a students group</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="65"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="71"/>
        <source>Replier toutes les classes</source>
        <translation>Fold down checkboxes</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="74"/>
        <source>Tout replier</source>
        <translation>Fold down everything</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="117"/>
        <source>Cocher tous les &#xe9;l&#xe8;ves visibles</source>
        <translation type="obsolete">Check any visible student&apos;s box</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="88"/>
        <source>Cocher</source>
        <translation>Check the box</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="119"/>
        <source>D&#xe9;cocher tous les &#xe9;l&#xe8;ves, visibles ou non</source>
        <translation type="obsolete">Unchez every student&apos;s box, either visible or not</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="120"/>
        <source>D&#xe9;cocher</source>
        <translation type="obsolete">Uncheck box</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="122"/>
        <source>Ajout &#xe0; la liste</source>
        <translation type="obsolete">Add to the list</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="135"/>
        <source>Suppr. de la liste</source>
        <translation>Del from the list</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="185"/>
        <source>Valider</source>
        <translation>Validate</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="126"/>
        <source>N&#xb0; &#xe0; partir de</source>
        <translation type="obsolete">Number from</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="125"/>
        <source>Quand la case est coch&#xe9;e, un num&#xe9;ro sera ajout&#xe9; comme pr&#xe9;fixe aux noms</source>
        <translation type="obsolete">When the box is checked, a number will be prepended to names</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="127"/>
        <source>La num&#xe9;rotation commence &#xe0; cette valeur</source>
        <translation type="obsolete">Numbering begins there</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="121"/>
        <source>Ajouter les noms coch&#xe9;s de l&apos;arbre dans la liste &#xe0; droite</source>
        <translation type="obsolete">Add checked names from the tree to the list on the right</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="132"/>
        <source>Supprimer de la liste les noms en surbrillance</source>
        <translation>Delete the list of highlighted names</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="182"/>
        <source>Accepter la liste actuelle et fermer ce dialogue</source>
        <translation>Accept the current list and close this dialog</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="192"/>
        <source>Supprimer tous les noms de la liste et fermer ce dialogue</source>
        <translation>Delete all the names from the list and close this dialog</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="131"/>
        <source>Remettre &#xe0; z&#xe9;ro</source>
        <translation type="obsolete">Reset</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="70"/>
        <source>Choix du propri&#xe9;taire</source>
        <translation type="obsolete">Choose the owner</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="71"/>
        <source>Nouveau nom du propri&#xe9;taire du baladeur</source>
        <translation type="obsolete">New name of the drive&apos;s owner</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="112"/>
        <source>Liste d&apos;&#xe9;l&#xe8;ves</source>
        <translation type="obsolete">List of students</translation>
    </message>
    <message>
        <location filename="../choixEleves.ui" line="26"/>
        <source>Ouvrir un fichier ...</source>
        <translation>Open a file...</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="94"/>
        <source>Fichier des &#xe9;l&#xe8;ves :</source>
        <translation type="obsolete">File of students:</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="67"/>
        <source>Nommer le baladeur nouvellement connect&#xe9;</source>
        <translation type="obsolete">Give a name to the recently connected drive</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="39"/>
        <source>Changez le choix parmi cette liste si une autre ligne convient mieux</source>
        <translation>Choose another line in the list if it fits better</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="56"/>
        <source>nom actuel du baladeur</source>
        <translation>current name of the drive</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="71"/>
        <source>C&apos;est le nom connu par votre ordinateur, s&apos;il a d&#xe9;j&#xe0; &#xe9;t&#xe9; d&#xe9;fini dans le pass&#xe9;</source>
        <translation type="obsolete">It is the name your computer knowed, if it was defined in the past</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="72"/>
        <source>nom propos&#xe9;</source>
        <translation type="obsolete">proposed name</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="73"/>
        <source>Le nouveau nom propos&#xe9; peut venir de la liste &#xe0; gauche ou &#xea;tre modifi&#xe9; &#xe0; la main</source>
        <translation type="obsolete">The proposed name can be taken from the list at the left side or be typed in</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="113"/>
        <source>Renommer le baladeur</source>
        <translation>Rename the drive</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="116"/>
        <source>Choisir comme nouveau nom</source>
        <translation>Choose as a new name</translation>
    </message>
    <message>
        <location filename="../nameAdrive.ui" line="123"/>
        <source>Fermer le dialogue sans rien faire</source>
        <translation>Close the dialog without doing anything</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="77"/>
        <source>&#xc9;chappement</source>
        <translation type="obsolete">Escape</translation>
    </message>
    <message>
        <location filename="../choixEleves.py" line="93"/>
        <source>Le fichier {schoolfile} n&apos;a pas pu &#xc3;&#xaa;tre trait&#xc3;&#xa9; : {erreur}</source>
        <translation type="unfinished">The file{schoolfile} could not be managed : {erreur} </translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="250"/>
        <source>La cle {id}&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation>The drive {id}&lt;br&gt;is not identified, give the owner&apos;s name</translation>
    </message>
    <message>
        <location filename="../preferences.py" line="50"/>
        <source>{t} secondes</source>
        <translation>{t} seconds</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>pas d&apos;action pour l&apos;attribut {a}</source>
        <translation>No action for attribute {a}</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="639"/>
        <source>La derni&#xe8;re commande &#xe9;tait&lt;br&gt;{cmd}&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs&#xa0;?</source>
        <translation type="obsolete">Last command was&lt;br&gt;{cmd}&lt;br&gt;Do you want to lauch it again with the new drives?</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="201"/>
        <source>Choissez un fichier (ou plus)</source>
        <translation>Please choose one or more files</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="205"/>
        <source>Choissez un r&#xc3;&#xa9;pertoire</source>
        <translation type="unfinished">Please choose a directory</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="220"/>
        <source>Aucune cl&#xc3;&#xa9; mod&#xc3;&#xa8;le s&#xc3;&#xa9;lectionn&#xc3;&#xa9;e</source>
        <translation type="unfinished">No template stick selected</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="503"/>
        <source>Choix de fichiers à supprimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="39"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Répertoire des &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documents de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="57"/>
        <source>Le répertoire des clés&lt;br&gt;où se trouvent les&lt;br&gt;documents de travail</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="60"/>
        <source>Le répertoire des clés où se trouvent les documents de travail</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="67"/>
        <source>Choix de clé modèle</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="83"/>
        <source>Choisir une des clés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="105"/>
        <source>Liste de fichiers à traiter (jokers autorisés)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="144"/>
        <source>Supprimer l&apos;item sélectionné</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="167"/>
        <source>Ajouter un fichier ou un filtre défini dans la ligne d&apos;édition</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="193"/>
        <source>Ligne d&apos;édition du nom d&apos;un fichier ou d&apos;un répertoire.&lt;br&gt;Jokers autorisés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../chooseInSticks.ui" line="232"/>
        <source>Rechercher (répertoire) ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="45"/>
        <source>Si la case est cochée,&lt;br&gt;les données sont transférées sur le disque dur&lt;br&gt;puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="48"/>
        <source>Si la case est cochée, les données sont transférées sur le disque dur puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="51"/>
        <source>Efface les fichiers et les répertoires après copie</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="77"/>
        <source>Fichier des élèves :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="91"/>
        <source>Répertoire pour les travaux :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../preferences.ui" line="105"/>
        <source>Fichier spécial pour le manuel :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="14"/>
        <source>Nommer le baladeur nouvellement connecté</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="66"/>
        <source>C&apos;est le nom connu par votre ordinateur, s&apos;il a déjà été défini dans le passé</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="86"/>
        <source>nom proposé</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="93"/>
        <source>Le nouveau nom proposé peut venir de la liste à gauche ou être modifié à la main</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../nameAdrive.ui" line="126"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="14"/>
        <source>Choix d&apos;un groupe d&apos;élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="20"/>
        <source>Liste d&apos;élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="85"/>
        <source>Cocher tous les élèves visibles</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="95"/>
        <source>Décocher tous les élèves, visibles ou non</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="98"/>
        <source>Décocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="118"/>
        <source>Ajouter les noms cochés de l&apos;arbre dans la liste à droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="121"/>
        <source>Ajout à la liste</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="162"/>
        <source>Quand la case est cochée, un numéro sera ajouté comme préfixe aux noms</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="165"/>
        <source>N° à partir de</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="172"/>
        <source>La numérotation commence à cette valeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../choixEleves.ui" line="195"/>
        <source>Remettre à zéro</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.ui" line="14"/>
        <source>Choix des fichiers à copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.ui" line="82"/>
        <source>Répertoire de destination des baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.ui" line="115"/>
        <source>Répertoire de destination sur les baladeurs,&lt;br&gt;peut être modifié dans les préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.ui" line="266"/>
        <source>Taille totale :</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="504"/>
        <source>Choix de fichiers à supprimer (jokers autorisés)</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="567"/>
        <source>Aucun fichier sélectionné</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="561"/>
        <source>Choix de fichiers à copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="562"/>
        <source>Choix de fichiers à copier depuis les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="575"/>
        <source>Choisir un répertoire de destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="606"/>
        <source>Voulez-vous voir les fichiers copiés ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="637"/>
        <source>Réitérer la dernière commande</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="637"/>
        <source>La dernière commande était&lt;br&gt;{cmd}&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../copyToDialog1.py" line="165"/>
        <source>%s méga-octets</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ownedUsbDisk.py" line="69"/>
        <source>Choix du propriétaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ownedUsbDisk.py" line="70"/>
        <source>Nouveau nom du propriétaire du baladeur</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>Main</name>
    <message>
        <location filename="../copyToDialog1.py" line="7274244"/>
        <source>D&#xe9;montage des baladeurs</source>
        <translation type="obsolete">Unmount the pluggable media</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="7274244"/>
        <source>&#xca;tes-vous s&#xfb;r de vouloir d&#xe9;monter tous les baladeurs coch&#xe9;s de la liste&#xa0;?</source>
        <translation type="obsolete">Do you really want to unmount all the pluggable media checked in the list?</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Cocher ou d&#xe9;cocher cette case en cliquant.</source>
        <translation type="obsolete">Chex or uncheck this box by clicking.</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Propri&#xe9;taire de la cl&#xe9; USB ou du baladeur&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour modifier.</source>
        <translation type="obsolete">Owner of the pluggable medium;&lt;br&gt;&lt;b&gt;Double click&lt;/b&gt; to modify.</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Point de montage de la cl&#xe9; USB ou du baladeur&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir les fichiers.</source>
        <translation type="obsolete">Mount point of the pluggable medium;&lt;br&gt;&lt;b&gt;Double click&lt;/b&gt; to see the files.</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Capacit&#xe9; de la cl&#xe9; USB ou du baladeur en kO&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occup&#xe9;e.</source>
        <translation type="obsolete">Size of the pluggable medium;&lt;br&gt;&lt;b&gt;Double click&lt;/b&gt; to see the used place.</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Fabricant de la cl&#xe9; USB ou du baladeur.</source>
        <translation type="obsolete">Manufacturer of the pluggable medium.</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Mod&#xe8;le de la cl&#xe9; USB ou du baladeur.</source>
        <translation type="obsolete">Model of the pluggable medium.</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Num&#xe9;ro de s&#xe9;rie de la cl&#xe9; USB ou du baladeur.</source>
        <translation type="obsolete">Serial number of the pluggable medium.</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Version num&#xe9;ro %1.%2</source>
        <translation type="obsolete">Version #%1.%2</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="774"/>
        <source>Propri&#xc3;&#xa9;taire de la cl&#xc3;&#xa9; USB ou du baladeur&#xc2;&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour modifier.</source>
        <translation type="unfinished">Check or uncheck this box bi a click&lt;br&gt;&lt;b&gt;Double click&lt;/b&gt; to modifiy several media.</translation>
    </message>
    <message>
        <location filename="../help.py" line="40"/>
        <source>Version num&#xc3;&#xa9;ro {major}.{minor}</source>
        <translation type="unfinished">Version number {major}.{minor}</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="671"/>
        <source>Démontage des baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="671"/>
        <source>Êtes-vous sûr de vouloir démonter tous les baladeurs cochés de la liste ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="772"/>
        <source>Cocher ou décocher cette case en cliquant.&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour agir sur plusieurs baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="776"/>
        <source>Point de montage de la clé USB ou du baladeur ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir les fichiers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="778"/>
        <source>Capacité de la clé USB ou du baladeur en kO ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occupée.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="780"/>
        <source>Fabricant de la clé USB ou du baladeur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="782"/>
        <source>Modèle de la clé USB ou du baladeur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="784"/>
        <source>Numéro de série de la clé USB ou du baladeur.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../diskFull.ui" line="14"/>
        <source>Disk size</source>
        <translation>Disk size</translation>
    </message>
    <message>
        <location filename="../diskFull.ui" line="25"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../diskFull.ui" line="39"/>
        <source>total size</source>
        <translation>total size</translation>
    </message>
    <message>
        <location filename="../diskFull.ui" line="46"/>
        <source>Used</source>
        <translation>Used</translation>
    </message>
    <message>
        <location filename="../mainWindow.ui" line="14"/>
        <source>ScolaSync</source>
        <translation>Scolasync</translation>
    </message>
    <message>
        <location filename="../mainWindow.ui" line="32"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="35"/>
        <source>Copier depuis les cles</source>
        <translation type="obsolete">Copy from the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="5"/>
        <source>Copier vers les cles</source>
        <translation type="obsolete">Copy to the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="7274514"/>
        <source>Supprimer dans les cl&#xe9;s</source>
        <translation type="obsolete">Delete in the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="0"/>
        <source>Demonter les cles</source>
        <translation type="obsolete">Unmount the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="0"/>
        <source>Preferences</source>
        <translation type="obsolete">Preferences</translation>
    </message>
    <message>
        <location filename="../mainWindow.ui" line="324"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="172"/>
        <source>Force &#xe0; recompter les baladeurs</source>
        <translation type="obsolete">Force medias count</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="170"/>
        <source>Affiche le nombre de baladeurs connect&#xe9;s</source>
        <translation type="obsolete">Displays medias count</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="136"/>
        <source>Copier depuis les baladeurs</source>
        <translation type="obsolete">Copy from medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="139"/>
        <source>Copier vers les baladeurs</source>
        <translation type="obsolete">Copy to medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="142"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs</source>
        <translation type="obsolete">Delete files or directories in medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="145"/>
        <source>Demonter les baladeurs</source>
        <translation type="obsolete">Unmount medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="175"/>
        <source>Pr&#xe9;f&#xe9;rences</source>
        <translation type="obsolete">Preferences</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="165"/>
        <source>Refaire &#xe0; nouveau</source>
        <translation type="obsolete">Redo last command</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="166"/>
        <source>Refaire &#xe0; nouveau la derni&#xe8;re op&#xe9;ration r&#xe9;ussie, avec les baladeurs connect&#xe9;s plus r&#xe9;cemment</source>
        <translation type="obsolete">Redo the last successful command, with newly connected medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="163"/>
        <source>&#xc9;jecter les baladeurs s&#xe9;lectionn&#xe9;s</source>
        <translation type="obsolete">Eject selected medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="154"/>
        <source>Copier depuis les baladeurs s&#xe9;lectionn&#xe9;s</source>
        <translation type="obsolete">Copy from selected medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="157"/>
        <source>Copier vers les baladeurs s&#xe9;lectionn&#xe9;s</source>
        <translation type="obsolete">Copy to selected medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="160"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs s&#xe9;lectionn&#xe9;s</source>
        <translation type="obsolete">Delete files or directories in selected medias</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="303"/>
        <source>Arr&#xea;ter les op&#xe9;rations en cours</source>
        <translation type="obsolete">Stop ongoing operations</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="304"/>
        <source>Essaie d&apos;arr&#xea;ter les op&#xe9;rations en cours. &#xc0; faire seulement si celles-ci durent trop longtemps</source>
        <translation type="obsolete">Try to stop ongoing operations. To do only if they are too slow</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="100"/>
        <source>&lt;br /&gt;Des noms sont disponibles pour renommer les prochains baladeurs que vous brancherez</source>
        <translation>&lt;br /&gt;Names are availble to rename next drives you will plug in</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="101"/>
        <source>&lt;br /&gt;Cliquez sur ce bouton pour pr&#xc3;&#xa9;parer une liste de noms afin de renommer les prochains baladeurs que vous brancherez</source>
        <translation type="unfinished">&lt;br /&gt;Click on this button to prepare a list of names to rename drives you will plug in later</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="44"/>
        <source>Copier depuis les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="71"/>
        <source>Copier vers les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="98"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="125"/>
        <source>Éjecter les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="299"/>
        <source>Refaire à nouveau</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="300"/>
        <source>Refaire à nouveau la dernière opération réussie, avec les baladeurs connectés plus récemment</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="250"/>
        <source>Affiche le nombre de baladeurs connectés</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="270"/>
        <source>Force à recompter les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.ui" line="297"/>
        <source>Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="301"/>
        <source>Arrêter les opérations en cours</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../mainWindow.py" line="302"/>
        <source>Essaie d&apos;arrêter les opérations en cours. À faire seulement si celles-ci durent trop longtemps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>checkBoxDialog</name>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="50"/>
        <source>Gestion des cases &#xe0; cocher</source>
        <translation type="obsolete">Checkbox manager</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="27"/>
        <source>Cocher tous les baladeurs</source>
        <translation>Check all the medias</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="30"/>
        <source>Tout cocher</source>
        <translation>Check all</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="55"/>
        <source>Inverser le choix de baladeurs&lt;br&gt;Les baladeurs coch&#xe9;s seront d&#xe9;coch&#xe9;s, et inversement</source>
        <translation type="obsolete">Toggle checks&lt;br&gt;Selected medias will be unselected, and vice versa</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="47"/>
        <source>Inverser le choix</source>
        <translation>Toggle the checks</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="58"/>
        <source>D&#xe9;s&#xe9;lectionner les baladeurs</source>
        <translation type="obsolete">Unselect the medias</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="64"/>
        <source>Ne rien cocher</source>
        <translation>Check none</translation>
    </message>
    <message>
        <location filename="../checkBoxDialog.ui" line="78"/>
        <source>Ne rien faire</source>
        <translation>Do nothing</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="62"/>
        <source>&#xc9;chappement</source>
        <translation type="obsolete">Escape</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../checkBoxDialog.ui" line="14"/>
        <source>Gestion des cases à cocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../checkBoxDialog.ui" line="44"/>
        <source>Inverser le choix de baladeurs&lt;br&gt;Les baladeurs cochés seront décochés, et inversement</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../checkBoxDialog.ui" line="61"/>
        <source>Désélectionner les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../checkBoxDialog.ui" line="81"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>diskFull</name>
    <message>
        <location filename="../Ui_mainWindow.py" line="7274400"/>
        <source>Place totale&#xa0;: %1 kilo-octets</source>
        <translation type="obsolete">Total size: %1 kBytes</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="7274400"/>
        <source>Place utilis&#xe9;e&#xa0;: %1 kilo-octets</source>
        <translation type="obsolete">Used size: %1 kBytes</translation>
    </message>
    <message>
        <location filename="../diskFull.py" line="49"/>
        <source>Place totale&#xc2;&#xa0;: {size} kilo-octets</source>
        <translation type="unfinished">Total size: {size} kilobytes</translation>
    </message>
    <message>
        <location filename="../diskFull.py" line="50"/>
        <source>Place utilis&#xc3;&#xa9;e&#xc2;&#xa0;: {size} kilo-octets</source>
        <translation type="unfinished">Used: {size} kilobytes</translation>
    </message>
</context>
<context>
    <name>uDisk</name>
    <message>
        <location filename="../usbDisk2.py" line="423"/>
        <source>point de montage</source>
        <translation>Mount point</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="424"/>
        <source>taille</source>
        <translation>Size</translation>
    </message>
    <message>
        <location filename="../usbDisk.py" line="1"/>
        <source>vendeur</source>
        <translation type="obsolete">Vendor</translation>
    </message>
    <message>
        <location filename="../usbDisk.py" line="7274450"/>
        <source>mod&#xe8;le de disque</source>
        <translation type="obsolete">Disk model</translation>
    </message>
    <message>
        <location filename="../usbDisk.py" line="7274450"/>
        <source>num&#xe9;ro de s&#xe9;rie</source>
        <translation type="obsolete">Serial number</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="430"/>
        <source>cocher</source>
        <translation>Check</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="201"/>
        <source>owner</source>
        <translation>Owner</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="425"/>
        <source>marque</source>
        <translation>vendor</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="284"/>
        <source>Partition ajout&#xe9;e %s</source>
        <translation type="obsolete">Partition added %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="300"/>
        <source>&#xc9;chec au montage du disque : %s</source>
        <translation type="obsolete">Failed to mount the disk: %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="331"/>
        <source>Disque ajout&#xe9; : %s</source>
        <translation type="obsolete">Disk added: %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="358"/>
        <source>Changement pour le disque %s</source>
        <translation>Change for the disk %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="370"/>
        <source>Disque d&#xe9;branch&#xe9; du syst&#xe8;me : %s</source>
        <translation type="obsolete">Disk unplugged from the system: %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="339"/>
        <source>On n&apos;ajoute pas le disque : partition non-USB</source>
        <translation>Not adding a disk: no USB partition</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="304"/>
        <source>On n&apos;ajoute pas le disque : partition vide</source>
        <translation>No disk added: empty partition</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="329"/>
        <source>Disque d&#xe9;j&#xe0; ajout&#xe9; auparavant : %s</source>
        <translation type="obsolete">Disk already added previously: %s</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="282"/>
        <source>Partition ajoutée %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="297"/>
        <source>Échec au montage du disque : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="326"/>
        <source>Disque déjà ajouté auparavant : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="328"/>
        <source>Disque ajouté : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="367"/>
        <source>Disque débranché du système : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="426"/>
        <source>modèle de disque</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../usbDisk2.py" line="427"/>
        <source>numéro de série</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
