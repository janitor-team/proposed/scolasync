var hierarchy =
[
    [ "src.gestClasse.AbstractGestClasse", "classsrc_1_1gestClasse_1_1AbstractGestClasse.html", [
      [ "src.gestClasse.Sconet", "classsrc_1_1gestClasse_1_1Sconet.html", null ]
    ] ],
    [ "src.notification.Notification", "classsrc_1_1notification_1_1Notification.html", null ],
    [ "QAbstractTableModel", "classQAbstractTableModel.html", [
      [ "src.mainWindow.usbTableModel", "classsrc_1_1mainWindow_1_1usbTableModel.html", null ]
    ] ],
    [ "QDialog", "classQDialog.html", [
      [ "src.checkBoxDialog.CheckBoxDialog", "classsrc_1_1checkBoxDialog_1_1CheckBoxDialog.html", null ],
      [ "src.choixEleves.choixElevesDialog", "classsrc_1_1choixEleves_1_1choixElevesDialog.html", null ],
      [ "src.chooseInSticks.chooseDialog", "classsrc_1_1chooseInSticks_1_1chooseDialog.html", null ],
      [ "src.copyToDialog1.copyToDialog1", "classsrc_1_1copyToDialog1_1_1copyToDialog1.html", null ],
      [ "src.help.helpWindow", "classsrc_1_1help_1_1helpWindow.html", null ],
      [ "src.nameAdrive.nameAdriveDialog", "classsrc_1_1nameAdrive_1_1nameAdriveDialog.html", null ],
      [ "src.preferences.preferenceWindow", "classsrc_1_1preferences_1_1preferenceWindow.html", null ]
    ] ],
    [ "QMainWindow", "classQMainWindow.html", [
      [ "src.diskFull.mainWindow", "classsrc_1_1diskFull_1_1mainWindow.html", null ],
      [ "src.mainWindow.mainWindow", "classsrc_1_1mainWindow_1_1mainWindow.html", null ],
      [ "src.ownedUsbDisk.MainWindow", "classsrc_1_1ownedUsbDisk_1_1MainWindow.html", null ],
      [ "src.usbDisk2.MainWindow", "classsrc_1_1usbDisk2_1_1MainWindow.html", null ]
    ] ],
    [ "QObject", "classQObject.html", [
      [ "src.ownedUsbDisk.uDisk2", "classsrc_1_1ownedUsbDisk_1_1uDisk2.html", null ]
    ] ],
    [ "QStyledItemDelegate", "classQStyledItemDelegate.html", [
      [ "src.mainWindow.CheckBoxDelegate", "classsrc_1_1mainWindow_1_1CheckBoxDelegate.html", null ],
      [ "src.mainWindow.DiskSizeDelegate", "classsrc_1_1mainWindow_1_1DiskSizeDelegate.html", null ],
      [ "src.mainWindow.UsbDiskDelegate", "classsrc_1_1mainWindow_1_1UsbDiskDelegate.html", null ]
    ] ],
    [ "QTextBrowser", "classQTextBrowser.html", [
      [ "src.mytextbrowser.myTextBrowser", "classsrc_1_1mytextbrowser_1_1myTextBrowser.html", null ]
    ] ],
    [ "QTreeView", "classQTreeView.html", [
      [ "src.gestclassetreeview.gestClasseTreeView", "classsrc_1_1gestclassetreeview_1_1gestClasseTreeView.html", null ]
    ] ],
    [ "src.sconet.Sconet", "classsrc_1_1sconet_1_1Sconet.html", null ],
    [ "Thread", null, [
      [ "src.usbThread.abstractThreadUSB", "classsrc_1_1usbThread_1_1abstractThreadUSB.html", [
        [ "src.usbThread.threadCopyFromUSB", "classsrc_1_1usbThread_1_1threadCopyFromUSB.html", null ],
        [ "src.usbThread.threadCopyToUSB", "classsrc_1_1usbThread_1_1threadCopyToUSB.html", null ],
        [ "src.usbThread.threadDeleteInUSB", "classsrc_1_1usbThread_1_1threadDeleteInUSB.html", null ],
        [ "src.usbThread.threadMoveFromUSB", "classsrc_1_1usbThread_1_1threadMoveFromUSB.html", null ]
      ] ]
    ] ],
    [ "src.usbThread.ThreadRegister", "classsrc_1_1usbThread_1_1ThreadRegister.html", null ],
    [ "src.usbDisk2.uDisk2", "classsrc_1_1usbDisk2_1_1uDisk2.html", [
      [ "src.ownedUsbDisk.uDisk2", "classsrc_1_1ownedUsbDisk_1_1uDisk2.html", null ]
    ] ],
    [ "src.usbDisk2.UDisksBackend", "classsrc_1_1usbDisk2_1_1UDisksBackend.html", [
      [ "src.usbDisk2.Available", "classsrc_1_1usbDisk2_1_1Available.html", [
        [ "src.ownedUsbDisk.Available", "classsrc_1_1ownedUsbDisk_1_1Available.html", null ]
      ] ]
    ] ]
];