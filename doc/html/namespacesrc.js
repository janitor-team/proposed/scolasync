var namespacesrc =
[
    [ "checkBoxDialog", "namespacesrc_1_1checkBoxDialog.html", "namespacesrc_1_1checkBoxDialog" ],
    [ "choixEleves", "namespacesrc_1_1choixEleves.html", "namespacesrc_1_1choixEleves" ],
    [ "chooseInSticks", "namespacesrc_1_1chooseInSticks.html", "namespacesrc_1_1chooseInSticks" ],
    [ "copyToDialog1", "namespacesrc_1_1copyToDialog1.html", "namespacesrc_1_1copyToDialog1" ],
    [ "diskFull", "namespacesrc_1_1diskFull.html", "namespacesrc_1_1diskFull" ],
    [ "gestClasse", "namespacesrc_1_1gestClasse.html", "namespacesrc_1_1gestClasse" ],
    [ "gestclassetreeview", "namespacesrc_1_1gestclassetreeview.html", "namespacesrc_1_1gestclassetreeview" ],
    [ "help", "namespacesrc_1_1help.html", "namespacesrc_1_1help" ],
    [ "mainWindow", "namespacesrc_1_1mainWindow.html", "namespacesrc_1_1mainWindow" ],
    [ "mytextbrowser", "namespacesrc_1_1mytextbrowser.html", "namespacesrc_1_1mytextbrowser" ],
    [ "nameAdrive", "namespacesrc_1_1nameAdrive.html", "namespacesrc_1_1nameAdrive" ],
    [ "notification", "namespacesrc_1_1notification.html", "namespacesrc_1_1notification" ],
    [ "ownedUsbDisk", "namespacesrc_1_1ownedUsbDisk.html", "namespacesrc_1_1ownedUsbDisk" ],
    [ "preferences", "namespacesrc_1_1preferences.html", "namespacesrc_1_1preferences" ],
    [ "sconet", "namespacesrc_1_1sconet.html", "namespacesrc_1_1sconet" ],
    [ "usbDisk2", "namespacesrc_1_1usbDisk2.html", "namespacesrc_1_1usbDisk2" ],
    [ "usbThread", "namespacesrc_1_1usbThread.html", "namespacesrc_1_1usbThread" ]
];