var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvw",
  1: "acdghmnpqstu",
  2: "s",
  3: "_cdghmnopstuv",
  4: "_abcdefghiklmnoprstuvw",
  5: "abcdefghilmnoprstuvw",
  6: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Espaces de nommage",
  3: "Fichiers",
  4: "Fonctions",
  5: "Variables",
  6: "Pages"
};

