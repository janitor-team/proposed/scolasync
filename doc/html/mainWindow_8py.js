var mainWindow_8py =
[
    [ "mainWindow", "classsrc_1_1mainWindow_1_1mainWindow.html", "classsrc_1_1mainWindow_1_1mainWindow" ],
    [ "usbTableModel", "classsrc_1_1mainWindow_1_1usbTableModel.html", "classsrc_1_1mainWindow_1_1usbTableModel" ],
    [ "CheckBoxDelegate", "classsrc_1_1mainWindow_1_1CheckBoxDelegate.html", "classsrc_1_1mainWindow_1_1CheckBoxDelegate" ],
    [ "UsbDiskDelegate", "classsrc_1_1mainWindow_1_1UsbDiskDelegate.html", "classsrc_1_1mainWindow_1_1UsbDiskDelegate" ],
    [ "DiskSizeDelegate", "classsrc_1_1mainWindow_1_1DiskSizeDelegate.html", "classsrc_1_1mainWindow_1_1DiskSizeDelegate" ],
    [ "CheckBoxRect", "mainWindow_8py.html#ae034587968568575779f6e856114f0c9", null ],
    [ "registerCmd", "mainWindow_8py.html#acd2ba9ca7936a8edd8a2d8e594813a17", null ],
    [ "access", "mainWindow_8py.html#a2ed2b78355f06891c75a93b176b273f6", null ],
    [ "activeThreads", "mainWindow_8py.html#ae62c35a7be3d7d4734788727fe4f736d", null ],
    [ "available", "mainWindow_8py.html#a65013dd608fe83ccd99b8d26e8ac1e20", null ],
    [ "lastCommand", "mainWindow_8py.html#ac2f9106c57f31f70e53af93e3c56b3f8", null ],
    [ "licence", "mainWindow_8py.html#a7c1c5829d4caf3120f11b4a33dc7e2ad", null ],
    [ "pastCommands", "mainWindow_8py.html#ac36f315b987717d7c8b267bf8aaf0834", null ]
];