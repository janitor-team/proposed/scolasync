/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ScolaSync", "index.html", [
    [ "But de l'application", "index.html#but", null ],
    [ "CAHIER DE CHARGES DE SCOLASYNC", "index.html#CahierDeCharges", null ],
    [ "Licence", "index.html#licence", null ],
    [ "Support", "index.html#support", null ],
    [ "Architecture de ScolaSync", "index.html#architecture", null ],
    [ "Paquetages", "namespaces.html", [
      [ "Paquetages", "namespaces.html", "namespaces_dup" ],
      [ "Fonctions de paquetage", "namespacemembers.html", [
        [ "Tout", "namespacemembers.html", null ],
        [ "Fonctions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Liste des classes", "annotated.html", "annotated_dup" ],
      [ "Index des classes", "classes.html", null ],
      [ "Hiérarchie des classes", "hierarchy.html", "hierarchy" ],
      [ "Membres de classe", "functions.html", [
        [ "Tout", "functions.html", "functions_dup" ],
        [ "Fonctions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Fichiers", "files.html", [
      [ "Liste des fichiers", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"____init_____8py.html",
"classsrc_1_1notification_1_1Notification.html#ad21c973847e274983156405a0bed70af",
"mainWindow_8py.html#a7c1c5829d4caf3120f11b4a33dc7e2ad"
];

var SYNCONMSG = 'cliquez pour désactiver la synchronisation du panel';
var SYNCOFFMSG = 'cliquez pour activer la synchronisation du panel';